package project_Activity_OrangeHRM;

import org.testng.annotations.Test;

import junit.framework.Assert;

import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

//Add an employee and their details to the site
public class Add_a_new_employee {
	WebDriver driver;
	WebDriverWait wait;
	String firstName = "Dinesh";
	String lastName = "Swamy";
	
	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,20);
		driver.manage().window().maximize();
		Reporter.log("Test Case Execution Started",true);
		Reporter.log("Opening the URL",true);
		driver.get("http://alchemy.hguy.co/orangehrm");
	}
	
	//login to the OrangeHRM portal
	@Test(priority=0)
	public void userLogin() {
		wait.until(ExpectedConditions.titleIs("OrangeHRM"));
		
		//find the username, password and submit button element and enter the value
		
		WebElement uName = driver.findElement(By.id("txtUsername"));
		WebElement password = driver.findElement(By.id("txtPassword"));
		WebElement submitButton = driver.findElement(By.id("btnLogin"));
		uName.sendKeys("orange");
		password.sendKeys("orangepassword123");
		submitButton.click();
		
		//Verify that the home page has opened
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("welcome")));
		String homePage = driver.findElement(By.id("welcome")).getText();
		Reporter.log("Login as: "+homePage, true);
		String substr = homePage.substring(0, 7);
		Assert.assertEquals("Welcome", substr);
	}
	
	//Find the PIM option in the menu and add employee details
	@Test(priority=1)
	public void add_New_Employee() {
		//find and click the PIM menu
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//b[contains(text(),'PIM')]")));
		WebElement PIMMenu = driver.findElement(By.xpath("//b[contains(text(),'PIM')]"));
		PIMMenu.click();
		
		//find and click the Add button to add employee details
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='btnAdd']")));
		driver.findElement(By.xpath("//input[@id='btnAdd']")).click();
		
		//Fill the employee details
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class = 'head']/h1")));
		driver.findElement(By.id("firstName")).sendKeys(firstName);
		driver.findElement(By.id("lastName")).sendKeys(lastName);
		driver.findElement(By.xpath("//input[@id='chkLogin']")).click();
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//label[contains(text(),'User Name')]")));
		driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys(firstName);
		driver.findElement(By.id("btnSave")).click();
		
		//Go to the Admin menu and verify if the user is created or not
		driver.findElement(By.xpath("//b[contains(text(),'Admin')]")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='toggle tiptip']")));
		driver.findElement(By.xpath("//input[@id='searchSystemUser_userName']")).sendKeys(firstName);
		driver.findElement(By.xpath("//input[@id='searchBtn']")).click();
		String element = driver.findElement(By.xpath("//a[contains(text(),'"+firstName+"')]")).getText();
		Assert.assertEquals(element, firstName);
		Reporter.log("User Created for: "+element, true);
	}

	@AfterClass
	public void afterClass() {
		Reporter.log("Test case Execution Completed", true);
		driver.close();
	}
}
