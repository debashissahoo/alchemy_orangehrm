package project_Activity_OrangeHRM;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

//Read the title of the website and verify the text
public class Verify_the_website_title {
	WebDriver driver;
	WebDriverWait wait;
	String titlePage;
	
	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,20);
		Reporter.log("Test Case Execution Started");
		Reporter.log("Opening the URL");
		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@Test
	public void verifyTitle() {
		wait.until(ExpectedConditions.titleIs("OrangeHRM"));
		titlePage = driver.getTitle();
		Reporter.log("Title of the Page is: "+titlePage, true);
		Assert.assertEquals("OrangeHRM", titlePage);
	
	}

	@AfterClass
	public void afterClass() {
		Reporter.log("Test Case Execution Completed");
		Reporter.log("Closing the browser");
		driver.close();
	}
}
