package project_Activity_OrangeHRM;

import org.testng.annotations.Test;

import junit.framework.Assert;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

//Edit a user�s information
public class Edit_user_information {
	WebDriver driver;
	WebDriverWait wait;
	String name = "Test1";
	String DOB = "1982-02-11";
	
	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,20);
		driver.manage().window().maximize();
		Reporter.log("Test Case Execution Started");
		Reporter.log("Opening the URL");
		driver.get("http://alchemy.hguy.co/orangehrm");
	}
	
	//login to the OrangeHRM portal
	@Test(priority=0)
	public void userLogin() {
		wait.until(ExpectedConditions.titleIs("OrangeHRM"));
		
		//find the username, password and submit button element and enter the value
		
		WebElement uName = driver.findElement(By.id("txtUsername"));
		WebElement password = driver.findElement(By.id("txtPassword"));
		WebElement submitButton = driver.findElement(By.id("btnLogin"));
		uName.sendKeys("orange");
		password.sendKeys("orangepassword123");
		submitButton.click();
		
		//Verify that the home page has opened
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("welcome")));
		String homePage = driver.findElement(By.id("welcome")).getText();
		Reporter.log("Login as: "+homePage, true);
		String substr = homePage.substring(0, 7);
		Assert.assertEquals("Welcome", substr);
	}
	
	//Find the �My Info� menu item and edit user information
	@Test(priority=1)
	public void editUserInformation() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//b[contains(text(),'My Info')]")));
		driver.findElement(By.xpath("//b[contains(text(),'My Info')]")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='btnSave']")));
		driver.findElement(By.xpath("//input[@id='btnSave']")).click();
		WebElement editName = driver.findElement(By.xpath("//input[@id='personal_txtEmpFirstName']"));
		editName.clear();
		editName.sendKeys(name);
		driver.findElement(By.xpath("//input[@id='personal_optGender_2']")).click();
		Select select = new Select(driver.findElement(By.xpath("//select[@id='personal_cmbNation']")));
		select.selectByVisibleText("Indian");
		WebElement dobElement = driver.findElement(By.xpath("//input[@id='personal_DOB']"));
		dobElement.clear();
		dobElement.sendKeys(DOB);
		driver.findElement(By.xpath("//input[@id='btnSave']")).click();
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
