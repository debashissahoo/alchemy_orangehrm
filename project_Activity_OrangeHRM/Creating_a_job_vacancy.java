package project_Activity_OrangeHRM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import junit.framework.Assert;

//To create a job vacancy for �DevOps Engineer�
public class Creating_a_job_vacancy {
	WebDriver driver;
	WebDriverWait wait;
	
	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,20);
		Reporter.log("Test Case Execution Started",true);
		Reporter.log("Opening the URL",true);
		driver.get("http://alchemy.hguy.co/orangehrm");
	}
	
	//Login to Web Site
	@Test(priority=0)
	public void userLogin() {
		wait.until(ExpectedConditions.titleIs("OrangeHRM"));
		
		//find the username, password and submit button element and enter the value
		
		WebElement uName = driver.findElement(By.id("txtUsername"));
		WebElement password = driver.findElement(By.id("txtPassword"));
		WebElement submitButton = driver.findElement(By.id("btnLogin"));
		uName.sendKeys("orange");
		password.sendKeys("orangepassword123");
		submitButton.click();
		
		//Verify that the home page has opened
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("welcome")));
		String homePage = driver.findElement(By.id("welcome")).getText();
		Reporter.log("Login as: "+homePage, true);
		String substr = homePage.substring(0, 7);
		Assert.assertEquals("Welcome", substr);
	}

	@Test(priority=1)
	public void create_JobVacancy() {
		//Navigate to the Recruitment page
		driver.findElement(By.xpath("//b[contains(text(),'Recruitment')]")).click();
		
		//Click on the �Vacancies� menu item to navigate to the vacancies page.
		driver.findElement(By.xpath("//a[@id='menu_recruitment_viewJobVacancy']")).click();
		
		//Click on the �Add� button to navigate to the �Add Job Vacancy� form.
		driver.findElement(By.xpath("//input[@id='btnAdd']")).click();
		
		//Fill out the necessary details.
		WebElement jobTitle = driver.findElement(By.xpath("//select[@id='addJobVacancy_jobTitle']"));
		Select job = new Select(jobTitle);
		job.selectByVisibleText("DevOps Engineer");
		driver.findElement(By.xpath("//input[@id='addJobVacancy_name']")).sendKeys("DevOps Engineer");
		driver.findElement(By.xpath("//input[@id='addJobVacancy_hiringManager']")).sendKeys("Debashis Sahoo");
		
		//Click the �Save� button to save the vacancy
		driver.findElement(By.xpath("//input[@id='btnSave']")).click();
		
		//Verify that the vacancy was created
		driver.findElement(By.xpath("//a[@id='menu_recruitment_viewJobVacancy']")).click();
		WebElement jobT = driver.findElement(By.xpath("//select[@id='vacancySearch_jobTitle']"));
		Select jobb = new Select(jobT);
		jobb.selectByVisibleText("DevOps Engineer");
		
		WebElement hm = driver.findElement(By.xpath("//select[@id='vacancySearch_hiringManager']"));
		Select selecthm = new Select(hm);
		selecthm.selectByVisibleText("Debashis Sahoo");
		driver.findElement(By.xpath("//input[@id='btnSrch']")).click();
		
		String verifyJobTitle = driver.findElement(By.xpath("//table/tbody/tr[1]/td[3]")).getText();
		Assert.assertEquals("DevOps Engineer", verifyJobTitle);
		
		String verifyHM = driver.findElement(By.xpath("//table/tbody/tr[1]/td[4]")).getText();
		Assert.assertEquals("Debashis Sahoo", verifyHM);
		
		String verifyStatus = driver.findElement(By.xpath("//table/tbody/tr[1]/td[5]")).getText();
		Assert.assertEquals("Active", verifyStatus);
		
		Reporter.log("Job Vacancy created for: "+verifyJobTitle, true);
		Reporter.log("Hiring Manager is: "+verifyHM, true);

	}
	@AfterClass
	public void afterClass() {
		Reporter.log("Test case execution completed",true);
		//Close the browser
		driver.close();
	}
}
