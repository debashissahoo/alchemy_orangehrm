package project_Activity_OrangeHRM;

import java.io.FileInputStream;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import junit.framework.Assert;

//Creating multiple vacancies using data from an external excel spreadsheet
public class Creating_multiple_vacancies {
	WebDriver driver;
	WebDriverWait wait;
	
	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,20);
		Reporter.log("Test Case Execution Started",true);
		Reporter.log("Opening the URL",true);
		driver.get("http://alchemy.hguy.co/orangehrm");
	}
	
	//Login to Web Site
	@Test(priority=0)
	public void userLogin() {
		wait.until(ExpectedConditions.titleIs("OrangeHRM"));
		
		//find the username, password and submit button element and enter the value
		
		WebElement uName = driver.findElement(By.id("txtUsername"));
		WebElement password = driver.findElement(By.id("txtPassword"));
		WebElement submitButton = driver.findElement(By.id("btnLogin"));
		uName.sendKeys("orange");
		password.sendKeys("orangepassword123");
		submitButton.click();
		
		//Verify that the home page has opened
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("welcome")));
		String homePage = driver.findElement(By.id("welcome")).getText();
		Reporter.log("Login as: "+homePage, true);
		String substr = homePage.substring(0, 7);
		Assert.assertEquals("Welcome", substr);
	}

	@Test(priority=1)
	public void create_JobVacancy() {
		try {
			FileInputStream inputFile = new FileInputStream("src/resources/Job Vacancy.xlsx");
			
			//Create WorkBook Instance to hold XLSX excel file
			XSSFWorkbook workbook = new XSSFWorkbook(inputFile);
			
			//Get first sheet from Workbook
			XSSFSheet sheet = workbook.getSheetAt(0);
			
			//Iterate each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();
			
			Row row = rowIterator.next();
			while(rowIterator.hasNext()) {
				row = rowIterator.next();
				//Navigate to the Recruitment page
				driver.findElement(By.xpath("//b[contains(text(),'Recruitment')]")).click();
				
				//Click on the �Vacancies� menu item to navigate to the vacancies page.
				driver.findElement(By.xpath("//a[@id='menu_recruitment_viewJobVacancy']")).click();
				
				//Click on the �Add� button to navigate to the �Add Job Vacancy� form.
				driver.findElement(By.xpath("//input[@id='btnAdd']")).click();
				
				//for each row, Iterate each column
				Iterator<Cell> cellIterator = row.cellIterator();
				while(cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					//Fill out the necessary details.
					WebElement jobTitle = driver.findElement(By.xpath("//select[@id='addJobVacancy_jobTitle']"));
					Select job = new Select(jobTitle);
					job.selectByVisibleText(cell.getStringCellValue());
					Cell cell2 = cellIterator.next();
					driver.findElement(By.xpath("//input[@id='addJobVacancy_name']")).sendKeys(cell2.getStringCellValue());
					Cell cell3 = cellIterator.next();
					driver.findElement(By.xpath("//input[@id='addJobVacancy_hiringManager']")).sendKeys(cell3.getStringCellValue());
					
					//Click the �Save� button to save the vacancy
					driver.findElement(By.xpath("//input[@id='btnSave']")).click();
					
				}
			}
			workbook.close();
			inputFile.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}	
	}
	
	//Verify that the vacancy was created
	@Test(priority=2)
	public void verify_JobVacancy() {
		
		try {
			FileInputStream inputFile = new FileInputStream("src/resources/Job Vacancy.xlsx");
			
			//Create WorkBook Instance to hold XLSX excel file
			XSSFWorkbook workbook = new XSSFWorkbook(inputFile);
			
			//Get first sheet from Workbook
			XSSFSheet sheet = workbook.getSheetAt(0);
			
			//Iterate each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();
			
			Row row = rowIterator.next();
			while(rowIterator.hasNext()) {
				row = rowIterator.next();
				//Navigate to the Recruitment page
				driver.findElement(By.xpath("//b[contains(text(),'Recruitment')]")).click();
				
				//Click on the �Vacancies� menu item to navigate to the vacancies page.
				driver.findElement(By.xpath("//a[@id='menu_recruitment_viewJobVacancy']")).click();
				
				//for each row, Iterate each column
				Iterator<Cell> cellIterator = row.cellIterator();
				while(cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					//Fill out the necessary details.
					WebElement jobT = driver.findElement(By.xpath("//select[@id='vacancySearch_jobTitle']"));
					Select jobb = new Select(jobT);
					jobb.selectByVisibleText(cell.getStringCellValue());
					Cell cell2 = cellIterator.next();
					WebElement vacancySearch = driver.findElement(By.xpath("//select[@id='vacancySearch_jobVacancy']"));
					Select vacancySelect = new Select(vacancySearch);
					vacancySelect.selectByVisibleText(cell2.getStringCellValue());
					Cell cell3 = cellIterator.next();
					WebElement hm = driver.findElement(By.xpath("//select[@id='vacancySearch_hiringManager']"));
					Select hmSelect = new Select(hm);
					hmSelect.selectByVisibleText(cell3.getStringCellValue());
					
					//Click the �Search� button to search the job vacancy
					driver.findElement(By.xpath("//input[@id='btnSrch']")).click();
					
					String verifyVacany = driver.findElement(By.xpath("//table/tbody/tr[1]/td[2]")).getText();
					Assert.assertEquals(cell2.getStringCellValue(), verifyVacany);
					
					String verifyJobTitle = driver.findElement(By.xpath("//table/tbody/tr[1]/td[3]")).getText();
					Assert.assertEquals(cell.getStringCellValue(), verifyJobTitle);
					
					String verifyHM = driver.findElement(By.xpath("//table/tbody/tr[1]/td[4]")).getText();
					Assert.assertEquals(cell3.getStringCellValue(), verifyHM);
					
					Reporter.log("Job Vacancy created for: "+verifyVacany, true);
					Reporter.log("Job Vacancy created for job title: "+verifyJobTitle, true);
					Reporter.log("Hiring Manager is: "+verifyHM, true);
				}
			 }
			workbook.close();
			inputFile.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@AfterClass
	public void afterClass() {
		Reporter.log("Test case execution completed",true);
		//Close the browser
		driver.close();
	}
}
