package project_Activity_OrangeHRM;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import junit.framework.Assert;

//Add multiple employees using an external CSV file
public class Add_multiple_employees {
	WebDriver driver;
	WebDriverWait wait;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 20);
		driver.manage().window().maximize();
		Reporter.log("Test Case Execution Started",true);
		Reporter.log("Opening the URL",true);
		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	// login to the OrangeHRM portal
	@Test(priority = 0)
	public void userLogin() {
		wait.until(ExpectedConditions.titleIs("OrangeHRM"));

		// find the username, password and submit button element and enter the value

		WebElement uName = driver.findElement(By.id("txtUsername"));
		WebElement password = driver.findElement(By.id("txtPassword"));
		WebElement submitButton = driver.findElement(By.id("btnLogin"));
		uName.sendKeys("orange");
		password.sendKeys("orangepassword123");
		submitButton.click();

		// Verify that the home page has opened
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("welcome")));
		String homePage = driver.findElement(By.id("welcome")).getText();
		Reporter.log("Login as: " + homePage, true);
		String substr = homePage.substring(0, 7);
		Assert.assertEquals("Welcome", substr);
	}

	// Find the PIM option in the menu and add employee details
	@Test(priority = 1)
	public void add_New_Employee() throws IOException, CsvException {

		// Load CSV file
		CSVReader reader = new CSVReader(new FileReader("src/resources/sample.csv"));

		// Load content in to list and get the no of rows
		List<String[]> list = reader.readAll();

		// Create Iterator reference
		Iterator<String[]> itr = list.iterator();

		// Iterate all the values
		itr.next();
		while (itr.hasNext()) {
			String[] data = itr.next();
			// find and click the PIM menu
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//b[contains(text(),'PIM')]")));
			WebElement PIMMenu = driver.findElement(By.xpath("//b[contains(text(),'PIM')]"));
			PIMMenu.click();

			// find and click the Add button to add employee details
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='btnAdd']")));
			driver.findElement(By.xpath("//input[@id='btnAdd']")).click();
			for (int i = 0; i < data.length; i++) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class = 'head']/h1")));
				driver.findElement(By.id("firstName")).sendKeys(data[i]);
				i++;
				driver.findElement(By.id("lastName")).sendKeys(data[i]);
				driver.findElement(By.xpath("//input[@id='chkLogin']")).click();
				i++;
				wait.until(ExpectedConditions
						.visibilityOfAllElementsLocatedBy(By.xpath("//label[contains(text(),'User Name')]")));
				driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys(data[i]);
			}

			driver.findElement(By.id("btnSave")).click();
		}
		reader.close();	
	}
	
	//Verify that the employees have been created
	@Test(priority=2)
	public void validate_Employee() throws IOException, CsvException {
		// Load CSV file
				CSVReader reader = new CSVReader(new FileReader("src/resources/sample.csv"));

				// Load content in to list and get the no of rows
				List<String[]> list = reader.readAll();

				// Create Iterator reference
				Iterator<String[]> itr = list.iterator();

				// Iterate all the values
				itr.next();
				while (itr.hasNext()) {
					String[] data = itr.next();
					// find and click the PIM menu
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//b[contains(text(),'PIM')]")));
					WebElement PIMMenu = driver.findElement(By.xpath("//b[contains(text(),'PIM')]"));
					PIMMenu.click();
					for (int i = 0; i < data.length-1; i++) {
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='empsearch_employee_name_empName']")));
						WebElement employee = driver.findElement(By.cssSelector("#empsearch_employee_name_empName"));
						employee.clear();
						employee.sendKeys(data[i]+" "+data[i+1]);
						driver.findElement(By.cssSelector("#searchBtn")).click();
						String firstName = driver.findElement(By.xpath("//table/tbody/tr/td[3]")).getText();
						String lastName = driver.findElement(By.xpath("//table/tbody/tr/td[4]")).getText();
						Assert.assertEquals(data[i],firstName );
						Assert.assertEquals(data[i+1], lastName);
						Reporter.log("Entry Created for employee: "+data[i]+" "+data[i+1],true);
						i++;
					}
				}
				reader.close();
	}

	@AfterClass
	public void afterClass() {
		Reporter.log("Test Case Execution Completed", true);
		//Close the browser
		driver.close();
	}
}
