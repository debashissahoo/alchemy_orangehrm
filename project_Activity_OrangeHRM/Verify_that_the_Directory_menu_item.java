package project_Activity_OrangeHRM;

import org.testng.annotations.Test;
import junit.framework.Assert;
import org.testng.annotations.BeforeClass;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Verify_that_the_Directory_menu_item {
	WebDriver driver;
	WebDriverWait wait;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 20);
		Reporter.log("Test Case Execution Started");
		Reporter.log("Opening the URL");
		driver.get("http://alchemy.hguy.co/orangehrm");
	}
	
	//login to the web site
	@Test(priority=0)
	public void userLogin() {
		wait.until(ExpectedConditions.titleIs("OrangeHRM"));

		// find the username, password and submit button element and enter the value
		WebElement uName = driver.findElement(By.id("txtUsername"));
		WebElement password = driver.findElement(By.id("txtPassword"));
		WebElement submitButton = driver.findElement(By.id("btnLogin"));
		uName.sendKeys("orange");
		password.sendKeys("orangepassword123");
		submitButton.click();

		// Verify that the home page has opened
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("welcome")));
		String homePage = driver.findElement(By.id("welcome")).getText();
		Reporter.log("Login as: " + homePage, true);
		String substr = homePage.substring(0, 7);
		Assert.assertEquals("Welcome", substr);
	}

	@Test(priority=1)
	public void verify_Directory_Menu() {
		//locate the navigation menu
		List<WebElement> navigationMenu = driver.findElements(By.xpath("//div[@class='menu']"));
		for(WebElement menuItem:navigationMenu) {
			Reporter.log("Menu items are",true);
			Reporter.log("----------------",true);
			Reporter.log(menuItem.getText(),true);
			
			//Verify that the �Directory� menu item is visible and clickable.
			if(menuItem.getText().contains("Directory")) {
				WebElement directoryMenu = driver.findElement(By.xpath("//b[contains(text(),'Directory')]"));
				Reporter.log("Directory menu item is displayed: "+directoryMenu.isDisplayed(),true);
				Reporter.log("Directory menu item is clickable: "+directoryMenu.isEnabled(),true);			
					directoryMenu.click();
				}
			}
		
		//Verify that the heading of the page matches �Search Directory�
		String Header = driver.findElement(By.xpath("//h1[contains(text(),'Search Directory')]")).getText();
		Assert.assertEquals("Search Directory",Header);
		Reporter.log("Heading of the page matches: "+Header, true);
	}
		
	@AfterClass
	public void afterClass() {
		//Close the browser
		driver.close();
	}


}
