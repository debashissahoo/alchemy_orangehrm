package project_Activity_OrangeHRM;

import java.io.File;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import junit.framework.Assert;

//Add information about a candidate for recruitment
public class Adding_a_candidate_for_recruitment {
	WebDriver driver;
	WebDriverWait wait;
	
	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,20);
		Reporter.log("Test Case Execution Started",true);
		Reporter.log("Opening the URL",true);
		driver.get("http://alchemy.hguy.co/orangehrm");
	}
	
	//Login to Web Site
	@Test(priority=0)
	public void userLogin() {
		wait.until(ExpectedConditions.titleIs("OrangeHRM"));
		
		//find the username, password and submit button element and enter the value
		
		WebElement uName = driver.findElement(By.id("txtUsername"));
		WebElement password = driver.findElement(By.id("txtPassword"));
		WebElement submitButton = driver.findElement(By.id("btnLogin"));
		uName.sendKeys("orange");
		password.sendKeys("orangepassword123");
		submitButton.click();
		
		//Verify that the home page has opened
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("welcome")));
		String homePage = driver.findElement(By.id("welcome")).getText();
		Reporter.log("Login as: "+homePage, true);
		String substr = homePage.substring(0, 7);
		Assert.assertEquals("Welcome", substr);
	}

	@Test(priority=1)
	public void create_JobVacancy() {
		//Navigate to the Recruitment page and click on the Add button to add candidate information
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//b[contains(text(),'Recruitment')]")));
		driver.findElement(By.xpath("//b[contains(text(),'Recruitment')]")).click();
		driver.findElement(By.cssSelector("#btnAdd")).click();
		
		//Fill in the details of the candidate
		driver.findElement(By.cssSelector("#addCandidate_firstName")).sendKeys("Debashis");
		driver.findElement(By.cssSelector("#addCandidate_lastName")).sendKeys("Sahoo");
		driver.findElement(By.cssSelector("#addCandidate_email")).sendKeys("deb@gmail.com");
		driver.findElement(By.cssSelector("#addCandidate_contactNo")).sendKeys("9123456789");
		WebElement jobVacancy = driver.findElement(By.cssSelector("#addCandidate_vacancy"));
		Select selectJobVacany = new Select(jobVacancy);
		selectJobVacany.selectByVisibleText("DevOps Engineer");
		
		//Upload a resume (docx or pdf) to the form
		File file = new File("src/resources/ResumeDebashis.docx");
		WebElement upload = driver.findElement(By.cssSelector("#addCandidate_resume"));
		upload.sendKeys(file.getAbsolutePath());
		
		//Click Save
		driver.findElement(By.cssSelector("#btnSave")).click();
		Reporter.log("Application submitted successfully", true);
		
		//Navigate back to the Recruitments page to confirm candidate entry
		driver.findElement(By.xpath("//b[contains(text(),'Recruitment')]")).click();
		WebElement jobT = driver.findElement(By.xpath("//select[@id='candidateSearch_jobTitle']"));
		Select jobb = new Select(jobT);
		jobb.selectByVisibleText("DevOps Engineer");
		
		WebElement hm = driver.findElement(By.xpath("//select[@id='candidateSearch_hiringManager']"));
		Select selecthm = new Select(hm);
		selecthm.selectByVisibleText("Debashis Sahoo");
		driver.findElement(By.xpath("//input[@id='btnSrch']")).click();
		
		String verifyJobTitle = driver.findElement(By.xpath("//table/tbody/tr[1]/td[2]")).getText();
		Assert.assertEquals("DevOps Engineer", verifyJobTitle);
		
		String verifyCandidate = driver.findElement(By.xpath("//table/tbody/tr[1]/td[3]")).getText();
		Assert.assertEquals("Debashis Sahoo", verifyCandidate);
		
		String verifyHM = driver.findElement(By.xpath("//table/tbody/tr[1]/td[4]")).getText();
		Assert.assertEquals("Debashis Sahoo", verifyHM);
		
		Reporter.log("Job Vacancy created for: "+verifyJobTitle, true);
		Reporter.log("Candidate name is: "+verifyCandidate, true);
		Reporter.log("Hiring Manager is: "+verifyHM, true);
	}
	@AfterClass
	public void afterClass() {
		Reporter.log("Test Case Execution Completed",true);
		//Close the driver
		driver.close();
	}
}
