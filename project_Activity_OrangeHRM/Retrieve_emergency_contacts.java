package project_Activity_OrangeHRM;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import junit.framework.Assert;

//Login and retrieve the emergency contacts for the user
public class Retrieve_emergency_contacts {
	WebDriver driver;
	WebDriverWait wait;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 20);
		Reporter.log("Test Case Execution Started",true);
		Reporter.log("Opening the URL",true);
		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@Test(priority=0)
	public void userLogin() {
		wait.until(ExpectedConditions.titleIs("OrangeHRM"));

		// find the username, password and submit button element and enter the value

		WebElement uName = driver.findElement(By.id("txtUsername"));
		WebElement password = driver.findElement(By.id("txtPassword"));
		WebElement submitButton = driver.findElement(By.id("btnLogin"));
		uName.sendKeys("orange");
		password.sendKeys("orangepassword123");
		submitButton.click();

		// Verify that the home page has opened
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("welcome")));
		String homePage = driver.findElement(By.id("welcome")).getText();
		Reporter.log("Login as: " + homePage, true);
		String substr = homePage.substring(0, 7);
		Assert.assertEquals("Welcome", substr);
	}

	@Test(priority=1)
	public void emergency_contacts() {
		//Navigate to the �My Info� page
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//b[contains(text(),'My Info')]")));
		driver.findElement(By.xpath("//b[contains(text(),'My Info')]")).click();
		
		//Click on the �Emergency Contacts� menu item
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'Emergency Contacts')]")));
		driver.findElement(By.xpath("//a[contains(text(),'Emergency Contacts')]")).click();
		
		//Retrieve information about all the contacts listed in the table
		List<WebElement> column = driver.findElements(By.xpath("//table/thead/tr/th"));
		List<WebElement> row = driver.findElements(By.xpath("//table/tbody/tr"));
		Reporter.log("Emergency contact details", true);
		Reporter.log("---------------------------------------------------------------", true);
		for(WebElement columnData:column) {
			System.out.print(" |"+columnData.getText());
			
		}
		Reporter.log("| Name | Relationship | Home Telephone | Mobile | Work Telephone |");
		Reporter.log("\n---------------------------------------------------------------",true);
		for(int i =1;i<=row.size();i++)
		{
			WebElement rows =driver.findElement(By.xpath("//table/tbody/tr[" +i+"]"));
			Reporter.log("  |"+ rows.getText()+ "  |",true);
		}
	}
	
	@AfterClass
	public void afterClass() {
		Reporter.log("\nTest case execution completed",true);
		//Close the browser
		driver.close();
	}
}
