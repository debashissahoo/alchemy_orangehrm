package project_Activity_OrangeHRM;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import junit.framework.Assert;

//Use OrangeHRM�s �CSV Data Import� function to create employees
public class Upload_a_CSV {
	WebDriver driver;
	WebDriverWait wait;
	Actions action;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 20);
		driver.manage().window().maximize();
		Reporter.log("Test Case Execution Started",true);
		Reporter.log("Opening the URL",true);
		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	// login to the OrangeHRM portal
	@Test(priority = 0)
	public void userLogin() {
		wait.until(ExpectedConditions.titleIs("OrangeHRM"));

		// find the username, password and submit button element and enter the value

		WebElement uName = driver.findElement(By.id("txtUsername"));
		WebElement password = driver.findElement(By.id("txtPassword"));
		WebElement submitButton = driver.findElement(By.id("btnLogin"));
		uName.sendKeys("orange");
		password.sendKeys("orangepassword123");
		submitButton.click();

		// Verify that the home page has opened
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("welcome")));
		String homePage = driver.findElement(By.id("welcome")).getText();
		Reporter.log("Login as: " + homePage, true);
		String substr = homePage.substring(0, 7);
		Assert.assertEquals("Welcome", substr);
	}

	// Upload a CSV
	@Test(priority = 1)
	public void add_New_Employee() throws IOException, CsvException {
		// Find the PIM option in the menu and click it
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//b[contains(text(),'PIM')]")));
		WebElement PIM = driver.findElement(By.xpath("//b[contains(text(),'PIM')]"));

		// Navigate to �Configuration -> Data Import� in the navigation menu and click
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='menu_pim_Configuration']")));
		WebElement config = driver.findElement(By.xpath("//a[@id='menu_pim_Configuration']"));
		Actions action = new Actions(driver);
		Actions move = action.moveToElement(PIM).moveToElement(config);
		move.build().perform();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@id='menu_admin_pimCsvImport']")));
		WebElement dataImport = driver.findElement(By.linkText("Data Import"));
		wait.until(ExpectedConditions.elementToBeClickable(dataImport));
		dataImport.click();

		// Upload the CSV file to this page
		File file = new File("src/resources/ImportData.csv");
		WebElement upload = driver.findElement(By.xpath("//input[@id='pimCsvImport_csvFile']"));
		upload.sendKeys(file.getAbsolutePath());

		// Click the upload button
		driver.findElement(By.xpath("//input[@id='btnSave']")).click();
		Reporter.log("Upload CSV successfully",true);
	}

	// Verify that the employees have been created
	@Test(priority = 2)
	public void validate_Employee() throws IOException, CsvException {
		// Load CSV file
		CSVReader reader = new CSVReader(new FileReader("src/resources/ImportData.csv"));

		// Load content in to list and get the no of rows
		List<String[]> list = reader.readAll();

		// Create Iterator reference
		Iterator<String[]> itr = list.iterator();

		// Iterate all the values
		itr.next();
		while (itr.hasNext()) {
			String[] data = itr.next();
			for (int i = 0; i < data.length-19; i++) {
				// find and click the PIM menu
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//b[contains(text(),'PIM')]")));
				driver.findElement(By.xpath("//b[contains(text(),'PIM')]")).click();
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='empsearch_employee_name_empName']")));
				WebElement employee = driver.findElement(By.cssSelector("#empsearch_employee_name_empName"));
				employee.clear();
				employee.sendKeys(data[i] + " " + data[i + 2]);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='searchBtn']")));
				driver.findElement(By.cssSelector("#searchBtn")).click();
				String firstName = driver.findElement(By.xpath("//table/tbody/tr/td[3]")).getText();
				String lastName = driver.findElement(By.xpath("//table/tbody/tr/td[4]")).getText();
				Assert.assertEquals(data[i], firstName);
				Assert.assertEquals(data[i + 2], lastName);
				Reporter.log("Entry Created for employee: " + data[i] + "  " + data[i + 2], true);
				i += 2;
			}
		}
		reader.close();
	}

	@AfterClass
	public void afterClass() {
		Reporter.log("Test case execution completed",true);
		//Close the browser
		driver.close();
	}
}
