package project_Activity_OrangeHRM;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

//Print the url of the header image to the console
public class Get_the_url_of_the_header_image {
	WebDriver driver;
	WebDriverWait wait;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,20);
		Reporter.log("Test Case Execution Started");
		Reporter.log("Opening the URL");
		driver.get("http://alchemy.hguy.co/orangehrm");
	}

	@Test
	public void get_URL_Header_Image() {
		wait.until(ExpectedConditions.titleIs("OrangeHRM"));
		WebElement imageHeader = driver.findElement(By.xpath("//div[@id = 'divLogo']/img"));
		String URL = imageHeader.getAttribute("src");
		Reporter.log("URL of the Header image is: "+URL, true);
	}

	@AfterClass
	public void afterClass() {
		Reporter.log("Test Case Execution Completed");
		Reporter.log("Closing the browser");
		driver.close();
	}

}
