package project_Activity_OrangeHRM;

import org.testng.annotations.Test;

import junit.framework.Assert;

import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

//Add employee qualifications
public class Adding_qualifications {
	WebDriver driver;
	WebDriverWait wait;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 20);
		Reporter.log("Test Case Execution Started",true);
		Reporter.log("Opening the URL",true);
		driver.get("http://alchemy.hguy.co/orangehrm");
	}
	
	//login to the web site
	@Test(priority=0)
	public void userLogin() {
		wait.until(ExpectedConditions.titleIs("OrangeHRM"));

		// find the username, password and submit button element and enter the value

		WebElement uName = driver.findElement(By.id("txtUsername"));
		WebElement password = driver.findElement(By.id("txtPassword"));
		WebElement submitButton = driver.findElement(By.id("btnLogin"));
		uName.sendKeys("orange");
		password.sendKeys("orangepassword123");
		submitButton.click();

		// Verify that the home page has opened
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("welcome")));
		String homePage = driver.findElement(By.id("welcome")).getText();
		Reporter.log("Login as: " + homePage, true);
		String substr = homePage.substring(0, 7);
		Assert.assertEquals("Welcome", substr);
	}

	@Test(priority=1)
	public void add_Employee_Qualification() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//b[contains(text(),'My Info')]")));
		driver.findElement(By.xpath("//b[contains(text(),'My Info')]")).click();
		
		//Scroll the page
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("window.scrollBy(0,600)");
		
		driver.findElement(By.xpath("//ul[@id='sidenav']//a[contains(text(),'Qualifications')]")).click();
		
		js.executeScript("window.scrollBy(400,0)");
		driver.findElement(By.cssSelector("#addWorkExperience")).click();
		
		WebElement company = driver.findElement(By.cssSelector("#experience_employer"));
		company.clear();
		company.sendKeys("IBM");
		Reporter.log("Company details entered", true);
		
		WebElement jobTitle = driver.findElement(By.cssSelector("#experience_jobtitle"));
		jobTitle.clear();
		jobTitle.sendKeys("Test Lead Engineer");
		Reporter.log("Job title entered", true);
		driver.findElement(By.xpath("//input[@id='btnWorkExpSave']")).click();
		
	}
	@AfterClass
	public void afterClass() {
		Reporter.log("Test Case Execution Completed",true);
		//Close the Browser
		driver.close();
	}

}
