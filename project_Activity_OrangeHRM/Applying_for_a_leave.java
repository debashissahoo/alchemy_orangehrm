package project_Activity_OrangeHRM;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import junit.framework.Assert;

//Login and apply for a leave on the HRM site
public class Applying_for_a_leave {
	WebDriver driver;
	WebDriverWait wait;
	
	
	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,20);
		Reporter.log("Test Case Execution Started",true);
		Reporter.log("Opening the URL",true);
		driver.get("http://alchemy.hguy.co/orangehrm");
	}
	//Login to Web site
	@Test(priority=0)
	public void userLogin() {
		wait.until(ExpectedConditions.titleIs("OrangeHRM"));
		
		//find the username, password and submit button element and enter the value
		
		WebElement uName = driver.findElement(By.id("txtUsername"));
		WebElement password = driver.findElement(By.id("txtPassword"));
		WebElement submitButton = driver.findElement(By.id("btnLogin"));
		uName.sendKeys("orange");
		password.sendKeys("orangepassword123");
		submitButton.click();
		
		//Verify that the home page has opened
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("welcome")));
		String homePage = driver.findElement(By.id("welcome")).getText();
		Reporter.log("Login as: "+homePage, true);
		String substr = homePage.substring(0, 7);
		Assert.assertEquals("Welcome", substr);
	}

	@Test(priority=1)
	public void apply_Leave() {
		//Navigate to the Dashboard page and click on the Apply Leave option
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//b[contains(text(),'Dashboard')]")));
		driver.findElement(By.xpath("//b[contains(text(),'Dashboard')]")).click();
		
		driver.findElement(By.xpath("//td[4]//div[1]//a[1]//img[1]")).click();
		
		//Select leave type and duration of the leave
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//select[@id='applyleave_txtLeaveType']")));
		WebElement leaveType = driver.findElement(By.xpath("//select[@id='applyleave_txtLeaveType']"));
		Select selectLeave = new Select(leaveType);
		selectLeave.selectByVisibleText("Paid Leave");
		WebElement fromDate = driver.findElement(By.xpath("//input[@id='applyleave_txtFromDate']"));
		WebElement toDate = driver.findElement(By.xpath("//input[@id='applyleave_txtToDate']"));
		WebElement comment = driver.findElement(By.xpath("//textarea[@id='applyleave_txtComment']"));
		
		//get the current date
		DateTimeFormatter  dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
		LocalDateTime now = LocalDateTime.now();  
		String todayDate = dtf.format(now);
		fromDate.clear();
		fromDate.sendKeys(todayDate);
		
		//Add 7 days to current date
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, 7);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String newDate = sdf.format(c.getTime());
		toDate.clear();
		toDate.sendKeys(newDate);
		comment.sendKeys("Emergency  Leave");
		
		//Submit the Leave
		driver.findElement(By.xpath("//input[@id='applyBtn']")).click();
		Reporter.log("Leave applied successfully",true);
		
		//Navigate to the My Leave page to check the status of leave application
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//b[contains(text(),'Dashboard')]")));
		driver.findElement(By.xpath("//b[contains(text(),'Dashboard')]")).click();
		driver.findElement(By.xpath("//span[contains(text(),'My Leave')]")).click();
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("window.scrollBy(600,0)");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='calFromDate']")));
		WebElement calFromDate = driver.findElement(By.xpath("//input[@id='calFromDate']"));
		calFromDate.clear();
		calFromDate.sendKeys(todayDate);
		WebElement calToDate=driver.findElement(By.xpath("//input[@id='calToDate']"));
		calToDate.clear();
		calToDate.sendKeys(newDate);
		driver.findElement(By.xpath("//input[@id='btnSearch']")).click();
		String leaveStaus = driver.findElement(By.xpath("//table/tbody/tr[1]/td[6]/a[1]")).getText();
		Reporter.log("Leave status is: "+leaveStaus, true);
		
	}
	@AfterClass
	public void afterClass() {
		Reporter.log("Test case execution completed",true);
		//Close the browser
		driver.close();
	}

}
